""" Module with http requests functions """


import logging
import aiohttp

import utils

log = logging.getLogger()

class Device:
    _config = None
    _uuid = None
    _session = None
    _device_udid = None

    def __init__(self, idx, config, host, uuid, verify_ssl):
        self._idx = idx
        self._config = config
        self._device_udid = utils.generate_device_udid()
        self._host = host
        self._uuid = uuid
        self._verify_ssl = verify_ssl

        connector = aiohttp.TCPConnector(verify_ssl=verify_ssl)
        self._session = aiohttp.ClientSession(connector=connector)

    def open(self):
        self._session.open()

    def close(self):
        self._session.close()

    def __del__(self):
        self.close()

    async def checkin_auth(self, config, uuid, session, device_udid):
        """Authenticate device on /checkin endpoint"""
        mdm_signature_auth, manifest_auth = utils.generate_manifest_auth(config, uuid, device_udid)
        headers = {'Mdm-Signature': mdm_signature_auth}
        async with session.put(config.get_checkin_url(), data=manifest_auth, headers=headers) as auth_resp:
            log.info(await auth_resp.text())

    async def checkin_token(self, config, uuid, session, device_udid):
        """Update device token on /checkin endpoint"""
        mdm_signature_token, manifest_token = utils.generate_manifest_token(config, uuid, device_udid)
        headers = {'Mdm-Signature': mdm_signature_token}
        async with session.put(config.get_checkin_url(), data=manifest_token, headers=headers) as token_resp:
            log.info(await token_resp.text())

    async def request_mdm(self, config, uuid, session, device_udid):
        """Request to /mdm endpoint with Idle status"""
        mdm_signature_mdm, manifest_mdm = utils.generate_manifest_mdm(config, uuid, device_udid)
        headers = {'Mdm-Signature': mdm_signature_mdm}
        async with session.put(config.get_server_url(), data=manifest_mdm, headers=headers) as mdm_resp:
            log.info(await mdm_resp.text())

    async def process_command(self, config, uuid, session, device_udid, resp):
        """Request with CommandUUID and additional device data to /mdm"""
        command_uuid = utils.get_command_uuid(resp)
        device_serial = utils.generate_device_serial()
        command_mdm_signature, manifest_mdm = utils.generate_command_mdm(config, uuid, device_udid, command_uuid, device_serial)
        headers = {'Mdm-Signature': command_mdm_signature}
        async with session.put(config.get_server_url(), data=manifest_mdm, headers=headers) as com_mdm_resp:
            log.info(await com_mdm_resp.text())

    async def command_uuid(self, config, uuid, session, device_udid):
        """Get CommandUUID"""
        mdm_signature_mdm, manifest_mdm = utils.generate_manifest_mdm(config, uuid, device_udid)
        headers = {'Mdm-Signature': mdm_signature_mdm}
        async with session.put(config.get_server_url(), data=manifest_mdm, headers=headers) as mdm_resp:
            resp = await mdm_resp.text()
            await self.process_command(config, uuid, session, device_udid, resp)

    async def auth_device(self, config, uuid, session, device_udid):
        await self.checkin_auth(config, uuid, session, device_udid)
        await self.checkin_token(config, uuid, session, device_udid)
        await self.request_mdm(config, uuid, session, device_udid)
        await self.command_uuid(config, uuid, session, device_udid)

    async def enroll(self):
        """ Enroll new device """

        config = self._config
        host   = self._host
        uuid   = self._uuid
        verify_ssl = self._verify_ssl

        url = '{}enroll/{}'.format(host, uuid)

        if not verify_ssl:
            log.warning('Not checking HTTPS SSL validity')

        connector = aiohttp.TCPConnector(verify_ssl=verify_ssl)
        async with aiohttp.ClientSession(connector=connector) as session:
            device_udid = utils.generate_device_udid()
            async with session.get(url) as resp:
                if resp.status == 200:
                    await config.write(await resp.text())
                    await self.auth_device(config, uuid, session, device_udid)
                else:
                    log.error('Connection status {}'.format(resp.status))

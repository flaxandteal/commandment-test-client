""" Module with main function """

import asyncio
import uvloop
import click
import config
import logging
import urllib
import concurrent.futures
from utils import generate_batch
from mdm_config import MDMConfig
from http_requests import Device
from server import APNServer

log = logging.getLogger()
asyncio.set_event_loop_policy(uvloop.EventLoopPolicy())

@click.command()
@click.option('--host', default=config.MDM_HOST_URL, help='Host IPaddress/domain')
@click.option('--uuid', default=config.UUID, help='User ID')
@click.option('--url', default=None, help='Full enrolment URL (overrides host/UUID)')
@click.option('--concurrency', default=1, help='Amount of concurrent requests')
@click.option('--count', default=1, help='Total amount of requests')
@click.option('--apns/--no-apns', default=False, help='Run a dummy APNS server')
@click.option('--apns-cert', default=None, help='Certificate PEM for APNS dummy server')
@click.option('--apns-key', default=None, help='Key for APNS dummy server certificate')
@click.option('--apns-key-pass', default=None, help='Password for APNS dummy server key')
@click.option('--verify-ssl/--no-verify-ssl', default=True, help='Ignore SSL verification during enrolment')
def run_testing(host, uuid, url, concurrency, count, apns, apns_cert, apns_key, apns_key_pass, verify_ssl):
    event_loop = asyncio.get_event_loop()

    if url:
        urlo = urllib.parse.urlparse(url)
        host = '{}://{}:{}/'.format(urlo.scheme, urlo.hostname, urlo.port)
        uuid = urlo.path.split('/')[-1]
        log.warning('Extracting host and UUID from URL: [%s] [%s]' % (host, uuid))

    config = MDMConfig(uuid, override_host=host)

    log.info('Server starting')

    server_coro = False
    if apns:
        if not apns_cert or not apns_key:
            raise RuntimeError("If running a dummy APNS, you must supply a certificate and keyfile")

        apns_server = APNServer()
        server_coro = event_loop.run_until_complete(apns_server.run(apns_cert, apns_key, apns_key_pass))

        log.info('Server started')

    async def new_device_enroll(i, j):
        device = Device('%d:%d' % (i, j), config, host, uuid, verify_ssl)
        #server.add_phone(phone)
        await device.enroll()

    for i, iteration in enumerate(generate_batch(count, concurrency)):
        croutines = [new_device_enroll(i, j) for j, _ in enumerate(iteration)]
        try:
            event_loop.run_until_complete(asyncio.gather(*croutines))
        except Exception:
            log.exception('Error on {} requests '.format(iteration))
            break

    log.info('Test finished')

    try:
        event_loop.run_forever()
    except KeyboardInterrupt:
        pass

    if server_coro:
        server_coro.close()
        event_loop.run_until_complete(server_coro.wait_closed())

    event_loop.close()

if __name__ == '__main__':
    # run test function
    run_testing()

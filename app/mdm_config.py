import os
import logging
import plistlib
import aiofiles
import base64
import urllib
from OpenSSL import crypto

log = logging.getLogger()

class MDMConfig:
    _plist = None
    _checkin_url = None
    _server_url = None

    def __init__(self, uuid, override_host=None):
        self._uuid = uuid
        self._override_host = override_host
        self._filename = '{}/xml_configs/{}.xml'.format(os.getcwd(), uuid)

    def _adjust_url(self, url):
        if self._override_host:
            urlo = urllib.parse.urlparse(url)
            url = self._override_host + urlo.path[1:]

        return url

    async def write(self, text):
        """ Write config response to XML file """
        async with aiofiles.open(self._filename, mode='w') as file:
            await file.write(text)
            await file.close()

    def load(self):
        """ Load PList from file for this device """

        plist_file = '{}/xml_configs/{}.xml'.format(os.getcwd(), self._uuid)
        try:
            self._plist = plistlib.readPlist(plist_file)
        except plistlib.InvalidFileException:
            log.error("Could not read file: %s" % plist_file)
            raise

        self._payload_content = self._plist.get('PayloadContent')

        self._payloads_by_type = {c['PayloadType']: c for c in self._payload_content}

        if len(self._payloads_by_type) < len(self._payload_content):
            log.warning("Configuration payload has multiple sub-payloads of the same type")

        mdm_config = self._payloads_by_type['com.apple.mdm']
        self._checkin_url = self._adjust_url(mdm_config['CheckInURL'])
        self._server_url = self._adjust_url(mdm_config['ServerURL'])
        self._topic = mdm_config['Topic']

    def warn_if_not_loaded(self):
        """ Print a warning if configuration is not yet loaded"""

        if not self._plist:
            log.warning("Configuration has not yet been loaded")

    def get_checkin_url(self):
        """ Get Checkin URL as given by the server """

        self.warn_if_not_loaded()

        return self._checkin_url

    def get_server_url(self):
        """ Get MDM server URL as given by the server """

        self.warn_if_not_loaded()

        return self._server_url

    def get_topic(self):
        """ Get topic for MDM push as given by the server """

        self.warn_if_not_loaded()

        return self._topic

    def create_signature(self, manifest):
        """ Create mdm-signature header """

        manifest = manifest.encode()

        if not self._plist:
            self.load()

        content = self._payloads_by_type['com.apple.security.pkcs12']
        pay = content['PayloadContent']
        key_pwd = content['Password']
        p12 = crypto.load_pkcs12(pay.data, key_pwd)
        algo = p12.get_certificate().get_signature_algorithm()
        pkey = p12.get_privatekey()
        signcert = p12.get_certificate()
        pkey = p12.get_privatekey()
        bio_in = crypto._new_mem_buf(manifest)
        PKCS7_DETACHED = 0x40
        pkcs7 = crypto._lib.PKCS7_sign(signcert._x509, pkey._pkey, crypto._ffi.NULL, bio_in, PKCS7_DETACHED)
        bio_out = crypto._new_mem_buf()
        crypto._lib.i2d_PKCS7_bio(bio_out, pkcs7)
        sigbytes = crypto._bio_to_string(bio_out)
        signature = base64.b64encode(sigbytes)

        return signature.decode()

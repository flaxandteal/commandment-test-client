""" Module with utils functions """

import plistlib
import base64
import uuid
import os


def generate_batch(iterable, n=1):
    iterable = range(0, iterable)
    l = len(iterable)
    for ndx in range(0, l, n):
        yield iterable[ndx:min(ndx + n, l)]


def get_command_uuid(plist):
    plist = plist.encode()
    xml_data = plistlib.readPlistFromBytes(plist)
    command_uuid = xml_data['CommandUUID']
    return command_uuid


def generate_fake_push_token():
    token = 'O2g1FQe/XtlrgM2YFHvRjUcvYEMTwn4UTPLImLzu+jZ=+'.encode()
    return base64.b64encode(token).decode()


def generate_device_udid():
    return str(uuid.uuid4()).replace('-', '')[:40]


def generate_device_serial():
    return str(uuid.uuid4()).replace('-', '')[:12].upper()


def generate_manifest_token(config, UUID, device_udid):
    fake_push_token = generate_fake_push_token()
    topic = config.get_topic()
    manifest_token = '<?xml version="1.0" encoding="UTF-8"?><!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd"><plist version="1.0"><dict><key>MessageType</key><string>TokenUpdate</string><key>Topic</key><string>{}</string><key>UUID</key><string>{}</string><key>UDID</key><string>{}</string><key>Token</key><data>{}</data></dict></plist>'.format(topic, UUID, device_udid, fake_push_token)
    return (config.create_signature(manifest_token), manifest_token)


def generate_manifest_auth(config, UUID, device_udid):
    manifest_auth = '<?xml version="1.0" encoding="UTF-8"?><!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd"><plist version="1.0"><dict><key>MessageType</key><string>Authenticate</string><key>UUID</key><string>{}</string><key>UDID</key><string>{}</string></dict></plist>'.format(UUID, device_udid)
    return (config.create_signature(manifest_auth), manifest_auth)


def generate_manifest_mdm(config, UUID, device_udid):
    topic = config.get_topic()
    manifest_mdm = '<?xml version="1.0" encoding="UTF-8"?><!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd"><plist version="1.0"><dict><key>Status</key><string>Idle</string><key>Topic</key><string>{}</string><key>UDID</key><string>{}</string></dict></plist>'.format(topic, device_udid)
    return (config.create_signature(manifest_mdm), manifest_mdm)


def generate_command_mdm(config, UUID, device_udid, command_uuid, device_serial):
    topic = config.get_topic()
    command_manifest_mdm = '<?xml version="1.0" encoding="UTF-8"?><!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd"><plist version="1.0"><dict><key>Status</key><string>Acknowledged</string><key>Topic</key><string>{}</string><key>UDID</key><string>{}</string><key>CommandUUID</key><string>{}</string><key>QueryResponses</key><dict><key>SerialNumber</key><string>{}</string></dict></dict></plist>'.format(topic, device_udid, command_uuid, device_serial)
    return (config.create_signature(command_manifest_mdm), command_manifest_mdm)

""" Module with server function """

import asyncio
import os
import ssl
import uvloop
import click
import config
import logging
import urllib
import json
from binascii import b2a_hex
from struct import unpack
from utils import generate_batch
from mdm_config import MDMConfig

log = logging.getLogger()
asyncio.set_event_loop_policy(uvloop.EventLoopPolicy())

def unpacked_uchar(data):
    return unpack('>B', data)

def unpacked_ushort_big_endian(data):
    return unpack('>H', data)[0]

def unpacked_uint_big_endian(data):
    return unpack('>I', data)[0]

def unpack_item(frame_data):
    item_len = unpacked_uint_big_endian(frame_data[1:5])

    n = 5

    if int(frame_data[n]) != 1:
        print("WARNING: Frame data incorrect")
    n += 1
    token_bin_length = unpacked_ushort_big_endian(frame_data[n:n+2])
    n += 2
    token_hex = b2a_hex(frame_data[n:n+token_bin_length])
    n += token_bin_length

    if int(frame_data[n]) != 2:
        print("WARNING: Frame data incorrect")
    n += 1
    payload_bin_length = unpacked_ushort_big_endian(frame_data[n:n+2])
    n += 2
    payload = json.loads(frame_data[n:n+payload_bin_length].decode('utf-8'))
    n += payload_bin_length

    if int(frame_data[n]) != 3:
        print("WARNING: Frame data incorrect")
    n += 1
    identifier_bin_length = unpacked_ushort_big_endian(frame_data[n:n+2])
    n += 2
    identifier = unpacked_uint_big_endian(frame_data[n:n+identifier_bin_length])
    n += identifier_bin_length

    if int(frame_data[n]) != 4:
        print("WARNING: Frame data incorrect")
    n += 1
    expiry_bin_length = unpacked_ushort_big_endian(frame_data[n:n+2])
    n += 2
    expiry = unpacked_uint_big_endian(frame_data[n:n+expiry_bin_length])
    n += expiry_bin_length

    if int(frame_data[n]) != 5:
        print("WARNING: Frame data incorrect")
    n += 1
    priority_bin_length = unpacked_ushort_big_endian(frame_data[n:n+2])
    n += 2
    priority = unpacked_uchar(frame_data[n:n+priority_bin_length])
    n += priority_bin_length

    item = {'token': token_hex, 'payload': payload, 'identifier': identifier, 'expiry': expiry, "priority": priority}

    return item


async def handle(reader, writer):
    while True:
        print("Received push notification")
        item_len_data = await reader.read(5)
        if not item_len_data:
            break

        item_len = unpacked_uint_big_endian(item_len_data[1:5])
        item_data = await reader.read(item_len)

        frame_data = item_len_data + item_data
        item = unpack_item(frame_data)
        print(item)

class APNServer:
    def add_phone(self):
        pass

    async def run(self, certfile, keyfile, keypass):
        ssl_context = ssl.SSLContext(ssl.PROTOCOL_SSLv23)
        ssl_context.options &= ~ssl.OP_NO_SSLv2
        ssl_context.options &= ~ssl.OP_NO_SSLv3
        ssl_context.load_cert_chain(certfile, keyfile=keyfile)

        coro = asyncio.start_server(handle, port=2195, ssl=ssl_context)
        return await coro

@click.command()
def run_server():
    event_loop = asyncio.get_event_loop()

    apns = APNServer()
    server = event_loop.run_until_complete(asyncio.ensure_future(apns.run()))

    try:
        event_loop.run_forever()
    except KeyboardInterrupt:
        pass

    server.close()
    event_loop.run_until_complete(server.wait_closed())
    event_loop.close()

if __name__ == '__main__':
    # run server function
    run_server()

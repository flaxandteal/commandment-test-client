run:
	cd app && python3 main.py

run-apns:
	mkdir -p certs
	if [ ! -f certs/.gitignore ]; then echo "*" > certs/.gitignore; fi
	if [ ! -f certs/cert.pem ]; \
	then \
		echo "*" > certs/.gitignore; \
		openssl req -x509 -newkey rsa:4096 -keyout certs/key.pem -out certs/cert.pem -days 365 -nodes -subj "/C=GB/ST=NI/L=Example/O=ExampleOrg/OU=APNSTeam/CN=APNSDummySever"; \
	fi
	cd app && python3 main.py --apns --apns-cert=${PWD}/certs/cert.pem --apns-key=${PWD}/certs/key.pem

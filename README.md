# gd-ios-emulator 

## Prerequisites

- Python 3.5 
- pip
- virtualenv (virtualenvwrapper is recommended for use during development)
- libffi-dev

## Installation

### 1. Create virtualenv

#### If you are using pyenv:

```
    pyenv virtualenv 3.5.2 client
    pyenv activate client
```

#### If you are using virtualenv:

```
    virtualenv .env
    source .env/bin/activate
```

### 2. Install dependencies

```
    pip install -r requirements.txt
```

### 3. Run project:

```
    make run
```
